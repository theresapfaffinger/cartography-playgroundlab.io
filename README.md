[![Cartography Playground](https://cartography-playground.gitlab.io/assets/cartography-playground-logo-line.svg)](https://cartography-playground.gitlab.io/)

https://cartography-playground.gitlab.io/

* **Forked** from [gitlab.com/pages/jekyll](https://gitlab.com/pages/jekyll)
* **Hosted** on [GitLab Pages](https://about.gitlab.com/features/pages/)
* **Based** on [Jekyll](https://jekyllrb.com/)
* **Built** by [GitLab CI/CD](https://about.gitlab.com/features/gitlab-ci-cd/)
* **Themed** with [Bootstrap](https://getbootstrap.com/)

---

### Dependencies

[![jekyll gem](https://img.shields.io/gem/v/jekyll.svg?label=jekyll+gem)](https://github.com/jekyll/jekyll/releases) 3.8.5

| Library Name                                                                       | Usage Location                       | Version (in use) | Version (latest)                                                                                                                                      |
| ---------------------------------------------------------------------------------- | ------------------------------------ | ---------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Material Design Icons](https://materialdesignicons.com/)                          | includes/head                        | 3.5.95           | [![CDNJS](https://img.shields.io/cdnjs/v/MaterialDesign-Webfont.svg)](https://cdnjs.com/libraries/MaterialDesign-Webfont)                             |
| [jQuery](https://jquery.com/)                                                      | includes/js                          | 3.3.1            | [![CDNJS](https://img.shields.io/cdnjs/v/jquery.svg)](https://cdnjs.com/libraries/jquery/)                                                            |
| [Popper.js (umd)](https://popper.js.org/)                                          | includes/js                          | 1.14.7           | [![CDNJS](https://img.shields.io/cdnjs/v/popper.js.svg)](https://cdnjs.com/libraries/popper.js)                                                       |
| [Bootstrap](https://getbootstrap.com/)                                             | includes/js  **and** css/scss        | 4.3.1            | [![CDNJS](https://img.shields.io/cdnjs/v/twitter-bootstrap.svg)](https://cdnjs.com/libraries/twitter-bootstrap)                                       |
| [MathJax](https://www.mathjax.org)                                                 | includes/mathjax                     | 2.7.5            | [![CDNJS](https://img.shields.io/cdnjs/v/mathjax.svg)](https://cdnjs.com/libraries/mathjax)                                                           |
| [SVG.js](http://svgjs.com/)                                                        | includes/svgjs                       | 2.7.1            | [![CDNJS](https://img.shields.io/cdnjs/v/svg.js.svg)](https://cdnjs.com/libraries/svg.js)                                                             |
| [Simple-Jekyll-Search](https://github.com/christian-fei/Simple-Jekyll-Search)      | search                               | 1.7.2            | [![CDNJS](https://img.shields.io/cdnjs/v/simple-jekyll-search.svg)](https://cdnjs.com/libraries/simple-jekyll-search)                                 |
| [wordcloud2.js](https://timdream.org/wordcloud2.js/#love)                          | search                               | 1.1.0            | [![CDNJS](https://img.shields.io/cdnjs/v/wordcloud2.js.svg)](https://cdnjs.com/libraries/wordcloud2.js)                                               |
| [Chart.js](http://www.chartjs.org/)                                                | playgrounds/contour-lines-to-profile | 2.7.3            | [![CDNJS](https://img.shields.io/cdnjs/v/Chart.js.svg)](https://cdnjs.com/libraries/Chart.js)                                                         |
| [Spectrum Colorpicker](https://bgrins.github.io/spectrum/)                         | playgrounds/mapdesign                | 1.8.0            | [![CDNJS](https://img.shields.io/cdnjs/v/spectrum.svg)](https://cdnjs.com/libraries/spectrum)                                                         |
| [Mapbox Gl JS](https://www.mapbox.com/mapbox-gl-js/api/)                           | includes/mapbox                      | 0.53.1           | [![jsdelivr](https://img.shields.io/npm/v/mapbox-gl.svg?colorB=007ec6)](https://www.jsdelivr.com/package/npm/mapbox-gl?path=dist)                     |
| [Mapbox Geocoder](https://www.mapbox.com/mapbox-gl-js/plugins)                     | includes/mapbox                      | 3.1.6            | [![jsdelivr](https://img.shields.io/npm/v/@mapbox/mapbox-gl-geocoder.svg)](https://www.jsdelivr.com/package/npm/@mapbox/mapbox-gl-geocoder?path=dist) |
| [Turf.js](http://turfjs.org/)                                                      | includes/mapbox                      | 5.1.5            | [![CDNJS](https://img.shields.io/cdnjs/v/Turf.js.svg)](https://cdnjs.com/libraries/Turf.js)                                                           |
| [Workbox](https://developers.google.com/web/tools/workbox/)                        | sw.js                                | 3.6.3            | [![Release](https://img.shields.io/github/release/GoogleChrome/workbox.svg)](https://github.com/GoogleChrome/workbox/releases)                        |
| [Google Maps JS API](https://developers.google.com/maps/documentation/)            | includes/googlemaps                  | v3               | [v3](https://developers.google.com/maps/documentation/javascript/reference/3.exp/?hl=de)                                                              |
| [Bing Maps Elevations API](https://msdn.microsoft.com/en-us/library/jj158959.aspx) | playgrounds/contour-lines-to-profile | v1               | [v1](https://msdn.microsoft.com/en-us/library/jj158961.aspx#URL%20Templates)                                                                          |
| [Open-Elevation API](https://open-elevation.com/)                                  | playgrounds/contour-lines-to-profile | v1               | [v1](https://github.com/Jorl17/open-elevation/blob/master/docs/api.md)                                                                                |
| [jekyll-compress-html](http://jch.penibelst.de/)                                   | layouts                              | 3.1.0            | [![Release](https://img.shields.io/github/release/penibelst/jekyll-compress-html.svg)](https://github.com/penibelst/jekyll-compress-html)             |


### Browser support

Internet Explorer is **not** supported

### Future TODOs
* Get rid of jQuery (Maybe?)
  * Bootstrap
    * possible release 4.3 without jQuery
      * https://github.com/twbs/bootstrap/pull/23586
  * Spectrum Color Picker
    * possible replacements
      * https://github.com/Simonwep/pickr
      * https://github.com/PitPik/colorPicker
      * https://tovic.github.io/color-picker/
      * https://github.com/jaames/iro.js
* Get rid of closed source dependencies
  * Google Maps/Bing Maps Elevation API
    * possible replacements
      * https://www.mapbox.com/help/access-elevation-data/
      * https://github.com/mcwhittemore/mapbox-elevation
      * http://docs.mapcat.com/services/height.html
* Get rid of external embeds
  * H5P
    * possible solution
      * https://github.com/tunapanda/h5p-standalone

### API Keys

* Google Maps JS API, Google Maps Elevation API  
  * [free plan](https://cloud.google.com/maps-platform/pricing/sheet/)
  * 40 000 elevation requests per month
* Bing Maps API, Bing Maps Elevations API
  * [free plan](https://www.microsoft.com/en-us/maps/create-a-bing-maps-key)
  * 125 000 requests per year
* Mapbox GL JS, Mapbox Geocoder  
  * [free plan](https://www.mapbox.com/pricing/)
  * 50 000 map views per month
  * 50 000 geocode requests per month

### GitLab CI/CD

This project's static Pages are built by *GitLab CI/CD*, following the steps defined in `.gitlab-ci.yml`.  

Every time something is committed to this repo, the build process is started.  
The build takes ~3min. Each month, there are 2000min free CI/CD.

### Local Development

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
2. Install [Ruby](https://www.ruby-lang.org/en/downloads/) or [Ruby Download Side](https://rubyinstaller.org/downloads/)
3. Install [Node.js](https://nodejs.org/en/)
4. Start "command prompt with ruby"-window with: <kbd>WIN+S</kbd> and <kbd>cmd</kbd>
  * Type in: `bundle install` (download and install dependencies)
  * Type in: `gem install jekyll bundler` (download and install dependencies)
  * Type in: `gem install execjs` (download and install dependencies)
5. Reboot ýour computer to complete your installation
6. "Start command prompt with ruby"-window again
  * Change into your project folder you have downloaded 
  * Type in: `bundle exec jekyll serve` (build locally and serve preview)
7. Open the web page into your Browser with: `localhost:4000` or `http://127.0.0.1:4000` 
8. Add content in your root folder and see the changes directly
9. Hard Reload in Browser with: <kbd>ctrl</kbd> + <kbd>F5</kbd>
10. End ruby server in "command prompt with ruby"-window with: <kbd>ctrl</kbd> + <kbd>C</kbd>
11. 💻 🌐 👍
